import edu.princeton.cs.algs4.WeightedQuickUnionUF;

public class Percolation {
    private boolean[][] grid;
    private int numOpenSites;
    private WeightedQuickUnionUF uf;
    private int n = 0;

    // creates n-by-n grid, with all sites initially blocked
    public Percolation(int n) {
        if(n <= 0) throw new IllegalArgumentException();
        grid = new boolean[n][n];
        uf = new WeightedQuickUnionUF(n*n+2);
        this.numOpenSites = 0;
        for (int i = 0; i < n; i++) {
            uf.union(i, n*n);
            int r1 = (n-1)*n+i;
            uf.union(r1,n*n+1);
        }

        this.n = n;
    }

    // opens the site (row, col) if it is not open already
    public void open(int row, int col) {
        if(row < 1 || row > n || col < 1 || col > n) throw new IllegalArgumentException();
        int x = row -1;
        int y = col -1;

        if(!grid[x][y]) {
            grid[x][y] = true;
            this.numOpenSites++;
            if (x - 1 >= 0 && grid[x-1][y]) {
                uf.union(x*n+y, (x-1)*n+y);
            }
            if (x + 1 < n && grid[x+1][y]) {
                uf.union(x*n+y, (x+1)*n+y);
            }
            if (y - 1 >= 0 && grid[x][y-1]) {
                uf.union(x*n+y, x*n+y-1);
            }
            if (y + 1 < n && grid[x][y+1]) {
                uf.union(x*n+y, x*n+y+1);
            }
        }

    }

    // is the site (row, col) open?
    public boolean isOpen(int row, int col) {
        if(row < 1 || row > n || col < 1 || col > n) throw new IllegalArgumentException();
        return grid[row - 1][col - 1];
    }

    // is the site (row, col) full?
    public boolean isFull(int row, int col) {
        if(row < 1 || row > n || col < 1 || col > n) throw new IllegalArgumentException();
        return grid[row - 1][col - 1] && uf.connected(n*n, row*n-n+col-1);
    }

    // returns the number of open sites
    public int numberOfOpenSites() {
        return this.numOpenSites;
    }

    // does the system percolate?
    public boolean percolates() {
        if(n == 1) return isOpen(1,1);
        else return uf.connected(n*n,n*n+1);
    }

    // test client (optional)
    public static void main(String[] args) {

    }
}